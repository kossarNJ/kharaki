from django.conf.urls import url
from django.urls import path

from . import views

urlpatterns = [
    # path('', views.index, name='index'),

    url(r'^$', views.show_tourist_panel, name="tourist_panel"),
]
