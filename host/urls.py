from django.conf.urls import url

from . import views

urlpatterns = [
    # path('', views.index, name='index'),

    url(r'^$', views.show_host_panel, name="host_panel"),
]
