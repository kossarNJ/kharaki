from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from tourist.models import Tourist
from user.models import UserMember


class Host(UserMember):
    is_host = models.BooleanField(default=True)  # TODO redundant field


# ------------- small field classes ----------------

class Facility(models.Model):
    name = models.CharField(max_length=70, primary_key=True)
    description = models.CharField(max_length=500)

    def __str__(self):
        return self.name + " " + self.description


class Sight(models.Model):
    name = models.CharField(max_length=200, primary_key=True)
    description = models.CharField(max_length=1000)

    def __str__(self):
        return self.name


# ------------------- main classes ------------------

class Residence(models.Model):
    host = models.ForeignKey(Host, on_delete=models.CASCADE)
    name = models.CharField(max_length=60)
    city = models.CharField(max_length=50)
    address = models.CharField(max_length=400)
    description = models.CharField(max_length=1000, blank=True)

    sights = models.ManyToManyField(Sight, through='SightResidenceDistance')
    facilities = models.ManyToManyField(Facility)

    comments = models.ManyToManyField(Tourist, through='Comment', related_name='Commenting')

    def __str__(self):
        return self.host.user.username + " : " + self.name


# class FormalResidence(Residence):
# TODO


class LocalResidence(Residence):
    consideration = models.CharField(max_length=500, blank=True)


class Room(models.Model):
    title = models.CharField(max_length=30)
    residence = models.ForeignKey(Residence, on_delete=models.CASCADE)
    capacity = models.PositiveIntegerField()
    price = models.PositiveIntegerField()
    room_description = models.CharField(max_length=1000, blank=True)

    reserves = models.ManyToManyField(Tourist, through='Reserve', related_name='Reservation')

    def __str__(self):
        return "room " + self.title + " of " + self.residence.name


# ---------------------- Relations ---------------------------

class SightResidenceDistance(models.Model):
    residence = models.ForeignKey(Residence, on_delete=models.CASCADE)
    sight = models.ForeignKey(Sight, on_delete=models.CASCADE)
    distance = models.CharField(max_length=15)

    def __str__(self):
        return "distance of " + self.sight.name + " from " + self.residence.name + " is " + self.distance


class Comment(models.Model):
    residence = models.ForeignKey(Residence, on_delete=models.CASCADE)
    tourist = models.ForeignKey(Tourist, on_delete=models.CASCADE)

    description = models.CharField(max_length=500, blank=True)
    rate = models.IntegerField(
        validators=[
            MaxValueValidator(5),
            MinValueValidator(0)
        ]
    )

    def __str__(self):
        return "comment of " + self.tourist.user.username + " for " + self.residence.name


class Availability(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    start_date = models.DateField()
    end_date = models.DateField()
    reason = (
        ('full', 'full room'),
        ('uc', 'under construction'),
        ('na', 'not available')
    )

    class Meta:
        unique_together = ('room', 'start_date', 'end_date')

    def __str__(self):
        return "room " + self.room.title + " of " + self.room.residence.name + \
               " is available from " + self.start_date.__str__() + " to " + self.end_date.__str__()


class Reserve(models.Model):
    tourist = models.ForeignKey(Tourist, on_delete=models.CASCADE)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)

    start_date = models.DateField()
    end_date = models.DateField()
    price = models.PositiveIntegerField()

    class Meta:
        unique_together = ('tourist', 'room', 'start_date', 'end_date')

    def __str__(self):
        return "reserve of " + self.room.__str__() + " for " + self.tourist.user.username
        # TODO other fields?
