from django.conf.urls import url
from django.urls import path

from . import views

urlpatterns = [

    url(r'^register/$', views.show_register),
    url(r'^register_success/$', views.show_register_success),
    url(r'^$', views.show_first_page , name="host_panel"),


    # path('register/', views.show_register),
    # path('register_success/', views.show_register_success),
]