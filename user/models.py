from django.contrib.auth.models import User
from django.db import models


# Create your models here.

class UserMember(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True,
        default=''
    )
    account_number = models.CharField(max_length=20)
    phone_number = models.CharField(max_length=17, blank=True)  # validators should be a list

    def __str__(self):
        return self.user.username

    class Meta:
        abstract = True


