from django.conf.urls import url
from django.urls import path

from . import views

urlpatterns = [
    # path('', views.index, name='index'),

    url(r'^$', views.show_manager_panel, name="manager_panel"),
]