from django.db import models

# Create your models here.
from user.models import UserMember


class Manager(UserMember):
    is_manager = models.BooleanField(default=True)  # TODO redundant field
